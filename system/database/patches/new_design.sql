ALTER TABLE `tbl_users`
  CHANGE `id` `user_id` INT(11) NOT NULL AUTO_INCREMENT
, CHANGE `firstName` `first_name` VARCHAR(225) NOT NULL
, CHANGE `middleName` `middle_name` VARCHAR(225) NULL DEFAULT NULL
, CHANGE `lastName` `last_name` VARCHAR(225) NOT NULL
, CHANGE `mobileNumber` `mobile` VARCHAR(50) NOT NULL
, CHANGE `dateOfBirth` `birth_date` DATE NOT NULL
, CHANGE `placeOfBirth` `birth_place` VARCHAR(100) NULL DEFAULT NULL
, CHANGE `currentResident` `current_resident` TEXT NULL DEFAULT NULL
, CHANGE `permeantOccupation` `permeant_occupation` VARCHAR(100) NULL DEFAULT NULL
, CHANGE `currentOccupation` `current_occupation` VARCHAR(100) NULL DEFAULT NULL
;