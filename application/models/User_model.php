<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model
{
	public function __construct()
	{
    parent::__construct();
  }

	public function get_login_user( $username, $password )
	{
		$username = trim( strtolower($username) );
		$password = md5( trim($password) );
		
		$sql = "SELECT
							user_id
						, first_name
						, last_name
						, username
						, email
						, is_system_password
						, is_profile_updated
						, image
						, gender
						, is_alive
						FROM tbl_users
						WHERE username = ?
						AND password = ?
						LIMIT 1
						";
		$query = $this->db->query( $sql, array($username, $password) );
		
		return ($query->num_rows() > 0) ? $query->row() : FALSE;
	}
	
	function get_userid_by_mobile( $mobile )
	{
		$sql = "SELECT user_id FROM tbl_users WHERE mobile = ? LIMIT 1";
		$query = $this->db->query( $sql, array($mobile) );
		
		return ( $query->num_rows() > 0 ) ? $query->row()->user_id : FALSE;
	}
	
	function generate_login_otp( $mobile )
	{
		$otp = $this->_generate_and_save_otp( $mobile, 'LOGIN' );
		
		$update_data 	= array( 'otp' => $otp );
    $where_data		= array( 'mobile' => $mobile );
			
		$this->db->update( 'tbl_users', $update_data, $where_data );
		
		return $otp;
	}
	
	function _generate_and_save_otp( $mobile, $type )
	{
		$this->load->helper('sms_helper');
		
		$digits = 4;
		$otp 		= str_pad( rand( 0, pow(10, $digits) -1 ), $digits, '0', STR_PAD_LEFT );
		
		switch( $type )
		{
			case 'LOGIN':
				send_otp_sms( $otp, $mobile );
			break;
		
			case 'FORGOT_PASSWORD':
				send_forgot_password_sms( $otp, $mobile );
			break;
		}
		
		return $otp;
	}
	
	function get_otp_login_user( $user_otp, $system_user )
	{
		$sql = "SELECT
							user_id
						, first_name
						, last_name
						, username
						, email
						, is_system_password
						, is_profile_updated
						, image
						, gender
						, is_alive
						FROM tbl_users
						WHERE otp = ?
						AND user_id = ?
						LIMIT 1
						";
		$query = $this->db->query( $sql, array($user_otp, $system_user) );
		
		return ($query->num_rows() > 0) ? $query->row() : FALSE;
	}
	
	function generate_forgot_password_otp( $mobile )
	{
		$otp = $this->_generate_and_save_otp( $mobile, 'FORGOT_PASSWORD' );
		
		$update_data 	= array( 'otp' => $otp );
    $where_data		= array( 'mobile' => $mobile );
			
		$this->db->update( 'tbl_users', $update_data, $where_data );
	}
	
	function get_user_by_username( $username )
	{
		$username = strtolower($username);
		
		$sql = "SELECT
							user_id
						, first_name
						, last_name
						, username
						, email
						, is_system_password
						, is_profile_updated
						, image
						, mobile
						, gender
						FROM tbl_users
						WHERE username = ?
						LIMIT 1
						";
		$query = $this->db->query( $sql, array($username) );
		
		return ($query->num_rows() > 0) ? $query->row() : FALSE;
	}
	
	function reset_password( $otp, $user_id, $user_pass )
	{
		$sql = "SELECT user_id FROM tbl_users WHERE user_id = ? AND otp = ? LIMIT 1";
		$query = $this->db->query( $sql, array($user_id, $otp) );
		
		if( $query->num_rows() > 0 )
		{
			$user_pass = md5( trim($user_pass) );
			
			$update_data 	= array( 'password' => $user_pass );
			$where_data		= array( 'user_id' => $user_id, 'otp' => $otp );
			
			$this->db->update( 'tbl_users', $update_data, $where_data );
			
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	function user_signup( $username, $user_email, $password, $native_place, $current_city )
	{
		$username = strtolower( trim($username) );
		$password = md5( $password );
		
		$insert_data = array(
				'username'       			=> $username
			, 'email'           		=> $user_email
			, 'password'           	=> $password
			, 'is_profile_updated' 	=> 0
			, 'address' 						=> $current_city
			, 'native_town' 				=> $native_place
			, 'createdAt'  	=> date('Y-m-d H:i:s')
		);
    
    $this->db->insert( 'tbl_users', $insert_data );
		
		$user = $this->get_user_by_username( $username );
		
		return $user;
	}
}