<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller
{
  public $is_guest = TRUE;
  public $user_id = 0;
  
	public function __Construct()
	{
		parent::__Construct();
    
    if( ($this->uri->segment('1') != 'profile' && $this->uri->segment('2') != 'logout') && ( $this->session->userdata('family_logged') && $this->session->userdata('is_profile_updated') == 0 ) )
    {
      $this->session->set_flashdata( 'profile_alert_msg', "Oops! Seems you still not updated your profile. Please complete all mandatory fields.");
      redirect('profile');
    }
    else if( ($this->uri->segment('1') != 'profile' && $this->uri->segment('2') != 'logout') && ( $this->session->userdata('family_logged') && $this->session->userdata('is_system_password') ) )
    {
      $this->session->set_flashdata( 'alert_msg', "You are still using password provided by the system. Please change your password.");
      redirect('profile/change_password');
    }
    
    $this->user_id    = $this->session->userdata('user_id');
    $this->is_guest   = ( $this->session->userdata('magicrelations_logged') ) ? FALSE : TRUE;
    //$this->detect     = new Mobile_Detect();
	}

	function _main( $data )
	{
    //$this->load->model('UserModel', 'user_model');
    
    $is_app = $this->input->get('app', TRUE);
    if( !is_numeric($is_app) )
    {
      $is_app = FALSE;    	
    }
    
    $data['base_url']   = base_url();
    $data['is_guest']   = ( $this->session->userdata('family_logged') ) ? FALSE : TRUE;
    $data['is_app']     = $is_app;
    $data['method']     = $this->uri->segment('2');
    $data['controller'] = $this->uri->segment('1');
		
		if( $this->session->userdata('family_logged') && ( $data['controller'] != 'profile' && $data['method'] != 'tree' ) )
		{
      $this->load->config('family');
      
      // get top 10 tags, recent tags, tending tags etc.
      if(  ($data['controller'] == 'home' && ($data['method'] =='' || $data['method'] == 'index' ) )
         || ($data['controller'] == 'posts' && $data['method'] == 'myposts')  )
      {
        
        //$data['top_ten_tags'] = $this->user_model->get_top_ten_tags();
        //$data['recent_tags'] = $this->user_model->get_recent_tags();
      }
      
      // Set user suggestion in session to avoid SQL queries on every page load
      /*$relations_suggestions = $this->session->userdata('suggest_relations');
      if( !isset( $relations_suggestions ) )
      {
        $this->load->library('suggest_relations');
        $relations_suggestions = $this->suggest_relations->get_relations_suggestions();
        
        if( !empty( $relations_suggestions['relatives'] ) || !empty( $relations_suggestions['other'] ) )
        {
          $this->session->set_userdata( 'suggest_relations', $relations_suggestions );
        }
      }
      
      $data['suggest_relations'] = $relations_suggestions;*/
		}
    
    $data['is_mobile']    = FALSE; //$this->detect->isMobile();
		$body_data['header_view']  = $this->load->view("main/header_view", $data, true);
		$body_data['footer_view']  = $this->load->view("main/footer_view", $data, true);
		
		$this->load->view( "main/main_view", $body_data );
	}
}