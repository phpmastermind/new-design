<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Main.php';

class Dashboard extends Main 
{
  function __Construct()
	{
		parent::__Construct();
    
    if( $this->is_guest )
    {
      redirect('login');
    }
  }
	
  function index()
	{
		echo "Welcome ".$this->session->userdata('username');
    echo "<br/>";
    echo "<a href='".base_url()."logout'>Logout</a>";
	}
  
}