<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Main.php';

class Account extends Main 
{
  function __Construct()
	{
		parent::__Construct();
    
    $this->load->model("user_model");
  }
	
  function login()
	{
    if( !$this->is_guest )
    {
      redirect('dashboard');
    }
    
		$data = array();
		$output['body_html'] = $this->load->view( 'account/login_view', $data, TRUE );

		$this->_main( $output );
	}
  
  function register()
  {
    if( !$this->is_guest )
    {
      redirect('dashboard');
    }
    
    $data = array();
		$output['body_html'] = $this->load->view( 'account/register_view', $data, TRUE );

		$this->_main( $output );
  }
  
  function user_login()
  {
    $username = $this->input->post("username", TRUE);
    $password = $this->input->post("password", TRUE);
    
    $this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
    
    if ($this->form_validation->run() == FALSE)
		{	
			echo json_encode( array("error" => "Please complete all fields") );
      exit;
		}
    
    $user = $this->user_model->get_login_user( $username, $password );
    
    $this->_validate_login( $user );
  }
  
  function login_send_otp()
  {
    $mobile = $this->input->post("mobile", TRUE);
    
    $this->form_validation->set_rules('mobile', 'mobile', 'trim|required|regex_match[/^[0-9]{10}$/]');
    
    if ($this->form_validation->run() == FALSE)
		{	
			echo json_encode( array("error" => "Please provide valid Mobile No.") );
      exit;
		}
    
    $user_id  = $this->user_model->get_userid_by_mobile( $mobile );
    
    if( !$user_id )
    {
      echo json_encode( array("error" => "Not found any user having this Mobile No.") );
      exit;
    }
    
    $otp = $this->user_model->generate_login_otp( $mobile );
    
    echo json_encode( array("user_id" => $user_id, "otp" => $otp) );
    exit;
  }
  
  function otp_login()
  {
    $user_otp     = $this->input->post("user_otp", TRUE);
    $system_otp   = $this->input->post("system_otp", TRUE);
    $system_user  = $this->input->post("system_user", TRUE);
    
    $this->form_validation->set_rules('user_otp', 'OTP', 'trim|required');
    
    if ($this->form_validation->run() == FALSE)
		{	
			echo json_encode( array("error" => "Please provide OTP") );
      exit;
		}
    
    if( $user_otp != $system_otp )
    {	
			echo json_encode( array("error" => "OTP you provided is not valid") );
      exit;
		}
    
    $user = $this->user_model->get_otp_login_user( $user_otp, $system_user );
    
    $this->_validate_login( $user );
  }
  
  function _validate_login( $user )
  {
    if( $user === FALSE )
    {
      echo json_encode( array("error" => "Invalid login details") );
      exit;
    }
    else
    {
      if( !$user->is_alive )
      {
        echo json_encode( array("error" => "Sorry! seems member is no more alive. You could not log into this account.") );
        exit;
      }
      
      $this->_auto_login( $user );
      
      $redirect = 'dashboard';
      
      if( $user->is_system_password )
      {
        $redirect = 'profile';
      }
      
      echo json_encode( array("path" => $redirect) );
      exit;
    }
  }
  
  function _auto_login( $user )
  {
    $this->session->set_userdata( 'user_id', $user->user_id );
    $this->session->set_userdata( 'username', ucfirst($user->username) );
    $this->session->set_userdata( 'first_name', $user->first_name );
    $this->session->set_userdata( 'last_name', $user->last_name );
    $this->session->set_userdata( 'email', $user->email );
    $this->session->set_userdata( 'is_system_password', $user->is_system_password );
    $this->session->set_userdata( 'is_profile_updated', $user->is_profile_updated );
    $this->session->set_userdata( 'magicrelations_logged', TRUE );
  }
  
  function forgot_password()
  {
    $username = $this->input->post("username", TRUE);
    
    $this->form_validation->set_rules('username', 'Username', 'trim|required');
    
    if ($this->form_validation->run() == FALSE)
		{	
			echo json_encode( array("error" => "Please provide Username") );
      exit;
		}
    
    $user = $this->user_model->get_user_by_username( $username );
    
    if( !$user )
    {
      echo json_encode( array("error" => "Not found any user having this Username") );
      exit;
    }
    
    $this->user_model->generate_forgot_password_otp( $user->mobile );
    
    echo json_encode( array("user_id" => $user->user_id) );
    exit;
  }
  
  function reset_password()
  {
    $otp            = $this->input->post("otp", TRUE);
    $user_id        = $this->input->post("user_id", TRUE);
    $user_pass      = $this->input->post("user_pass", TRUE);
    $user_pass_con  = $this->input->post("user_pass_con", TRUE);
    
    $this->form_validation->set_rules('otp', 'OTP', 'trim|required');
    $this->form_validation->set_rules('user_pass', 'User Password', 'trim|required');
    $this->form_validation->set_rules('user_pass_con', 'User Password Confirm', 'trim|required');
    
    if ($this->form_validation->run() == FALSE)
		{	
			echo json_encode( array("error" => "Please complete all fields") );
      exit;
		}
    
    if( $user_pass != $user_pass_con )
    {
      echo json_encode( array("error" => "Password and Confirm Password are not same") );
      exit;
    }
    
    $res = $this->user_model->reset_password( $otp, $user_id, $user_pass );
    
    if( $res )
    {
      echo json_encode( array("success" => 1) );
      exit;
    }
    else
    {
      echo json_encode( array("error" => "OTP provided is not valid") );
      exit;
    }
  }
  
  function user_signup()
  {
    $username       = $this->input->post("username", TRUE);
    $user_email     = $this->input->post("user_email", TRUE);
    $password       = $this->input->post("password", TRUE);
    $conf_password  = $this->input->post("conf_password", TRUE);
    $native_place   = $this->input->post("native_place", TRUE);
    $current_city   = $this->input->post("current_city", TRUE);
    
    $this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[3]');
    $this->form_validation->set_rules('user_email', 'Email', 'trim|required|valid_email');
    $this->form_validation->set_rules('password', 'Pasword', 'trim|required');
    $this->form_validation->set_rules('conf_password', 'Confirm Password', 'trim|required|matches[password]');
    $this->form_validation->set_rules('native_place', 'Native Place', 'trim|required');
    $this->form_validation->set_rules('current_city', 'Current City', 'trim|required');
    
    if ($this->form_validation->run() == FALSE)
		{	
			echo json_encode( array("error" => validation_errors()) );
      exit;
		}
    else
    {
      $user = $this->user_model->get_user_by_username( $username );
      
      if( $user || ($user && $user->email == $user_email) )
      {
        echo json_encode( array("error" =>'User already exists with this username or email.') );
        exit;
      }
      
      $new_user = $this->user_model->user_signup( $username, $user_email, $password, $native_place, $current_city );
      $this->_auto_login( $new_user );
      
      //echo json_encode( array("path" => 'profile') );
      echo json_encode( array("path" => 'dashboard') );
      exit;
    }
    
  }
  
  function logout()
  {
    $this->session->sess_destroy();
    
    redirect('login');
  }
  
}