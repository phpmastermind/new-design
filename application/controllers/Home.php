<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Main.php';

class Home extends Main 
{
  function __Construct()
	{
		parent::__Construct();
  }
	
  function index()
	{
		$data = array();
		$output['body_html'] = $this->load->view( 'home_view', $data, TRUE );

		$this->_main( $output );
	}
  
  function welcome()
  {
    $data = array();
		$output['body_html'] = $this->load->view( 'welcome_view', $data, TRUE );

		$this->_main( $output );
  }
  
}