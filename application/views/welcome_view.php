<section id="welcome-1" class="clearfix">
  <div class="container h-100">
    <div class="row justify-content-center align-self-center">
      <div class="col-md-12 intro-info order-md-first order-last text-center">
        <h2>Welcome</h2>
        <h3>The best way to meet family members and<br> try new activities. Let's get started!</h3>
        <div>
          <a href="<?php echo base_url();?>login" class="btn-get-started scrollto">NEXT</a>
        </div>
      </div>
  </div>
  </div>
</section>