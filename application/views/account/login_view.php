<section id="intro" class="clearfix">
  <div class="container h-100">
    <div class="row justify-content-center align-self-center">
      <div class="col-md-12 intro-info animated fadeInDown delay-5s">
        <h2>Welcome</h2>
        <h3>The best way to meet family members and<br> try new activities. Let's get started!</h3>
        
        <div>
          <a href="<?php echo base_url();?>register" class="btn-get-started scrollto">register</a>
        </div>
      </div>
    </div>
    
    <div class="row justify-content-center align-self-center">
      <div class="col-md-12 col-sm-12 intro-info order-md-first order-last animated fadeInUp delay-5s">
        <div id="msform">
          <!-- fieldsets -->
          <fieldset class="formloginReg">
            <div class="loginyour-account" id="div4">
              <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" class="custom-control-input" id="customRadio" name="example" value="customEx" onclick="show1();" checked>
                <label class="custom-control-label" for="customRadio">Password Login</label>
              </div>
              
              <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" class="custom-control-input" id="customRadio2" name="example" value="customEx" onclick="show2();">
                <label class="custom-control-label" for="customRadio2">OTP Login</label>
              </div>
            </div>
            
              <div id="div1" class="">
                <div id="" class="goRight">
                  <form class="loginForm">
                    <div class="input-fields-div autoMargin">
                      <div class="input-field">
                        <input id="username" type="text" class="validate" required="">
                        <label for="username">Username</label>
                      </div>
                      
                      <div id="passwordDiv" class="input-field ">
                        <input id="password" type="password" class="validate" required="">
                        <label for="password">Password</label>
                        <a href="javascript:void(0)" class="showPassword" onclick="showPassword()"><i class="material-icons md-18">visibility_off</i></a>
                      </div>
                      
                      <div class="alert alert-danger hide"></div>
                      
                      <p><a href="javascript:void(0)" onclick="show3();">Forgot Password?</a></p>
                      <p><a href="<?php echo base_url();?>register">Create an account</a></p>
                      
                      <button type="button" id="login_user" class="btn btn-block button">Login</button>
                    </div>
                  </form>
                </div>
              </div>
              
              <div id="div3" class="hidden">
                <div id="" class="goRight">
                  <h6>FORGOT PASSWORD</h6>
                  
                  <form class="loginForm" id="forgot_password">
                    <div class="input-fields-div autoMargin">
                      <div class="input-field">
                        <input id="user_name_fg" type="text" class="validate" required="">
                        <label for="user_name_fg">Username</label>
                      </div>
                      
                      <div class="alert alert-danger hide"></div>
                      
                      <button type="button" id="submit_forgot_password" class="btn btn-block button">Submit</button>
                      <p class="mt-auto p-2 d-flex align-content-start flex-wrap mb-0"><a href="javascript:void(0)" onclick="show1()"><i class="ion-android-arrow-back"></i> Login</a></p>
                    </div>
                  </form>
                </div>
              </div>
              
              <div id="div5" class="hidden">
                <div class="goRight">
                  <h6>RESET PASSWORD</h6>
                  
                  <form class="loginForm" id="reset_password" onsubmit="show1()">
                    <div class="input-fields-div autoMargin">
                      <div class="input-field">
                        <input id="user_otp" type="tel" class="validate" required="">
                        <label for="user_otp">OTP</label>
                      </div>
                      
                      <div class="input-field">
                        <input id="user_pass" type="password" class="validate" required="">
                        <label for="user_pass">New Password</label>
                      </div>
                      
                      <div class="input-field">
                        <input id="user_pass_con" type="password" class="validate" required="">
                        <label for="user_pass_con">Confirm Password</label>
                      </div>
                      
                      <div class="alert alert-danger hide"></div>
                      
                      <input type="hidden" id="user_id" value="" />
                      <button type="button" id="reset_password_btn" class="btn btn-block button mt-3">Reset</button>
                      <p class="mt-auto p-2 d-flex align-content-start flex-wrap mb-0"><a href="javascript:void(0)" onclick="show1()"><i class="ion-android-arrow-back"></i> Login</a></p>
                    </div>
                  </form>
                </div>
              </div>
              
              <div id="div2" class="hidden ">
                <div id="div6">
                  <form class="login-send-otp">
                    <div class="col-xs-12">
                      <div class="input-field">
                        <input id="user_phone" type="tel" class="validate">
                        <label for="user_phone">Mobile No.</label>
                      </div>
                      <div class="alert alert-danger hide"></div>
                      <button class="btn btn-lg button" id="send_otp" type="button">Send OTP</button>
                    </div>
                  </form>
                </div>
                
                <div id="div7">
                  <form id="login_with_otp">
                    <div class="col-xs-12">
                      <div class="input-field">
                        <input id="user_otp_fg" type="tel" class="validate" required="">
                        <label for="user_otp_fg">Enter OTP</label>
                      </div>
                      <div class="alert alert-danger hide"></div>
                      
                      <p><a href="javascript:void(0)" id="resend_login_otp">Resend OTP</a></p>
                      
                      <input type="hidden" id="login_otp" value="" />
                      <input type="hidden" id="login_otp_user" value="" />
                      <button class="btn btn-block nextBtn btn-lg button" id="login_by_otp" type="button">Submit OTP</button>
                    </div>
                  </form>
                </div>
              </div>
          </fieldset>
        </div>
      </div>
    </div>
    
    <div>
    </div>
  </div>
</section>