<section id="register" class="clearfix">
  <div class="container h-100">
    <div class="row justify-content-center align-self-center">
      <div class="col-md-4 intro-info order-md-first order-last">
        <a href="#"><img src="<?php echo base_url();?>assets/img/logo.png" class="img-fluid"></a>
      </div>
    </div>
    
    <div class="row justify-content-center align-self-center mt-40">
      <div class="col-md-12  col-sm-12 intro-info order-md-first order-last">
        <form id="msform" class="formloginReg user_signup_form">
            <!-- progressbar -->
          <ul id="progressbar" style="display: none;">
            <li class="active">Account Setup</li>
            <li>Social Profiles</li>
          </ul>
          
          <!-- fieldsets -->
          <fieldset id="step_one">
            <h6 class="fs-title">SIGN UP NOW!</h6>
            <div class="input-fields-div autoMargin">
              <div class="input-field">
                <input id="user_name" type="text" class="validate" required="">
                <label for="user_name">Username</label>
              </div>
              <div class="input-field">
                <input id="user_email" type="email" class="validate" required="">
                <label for="user_email">Email</label>
              </div>
              <div id="passwordDiv" class="input-field ">
                <input id="pass_word" type="password" class="validate">
                <label for="pass_word">Password</label>
                <a href="javascript:void(0)" class="showPassword" onclick="showPassword()"><i class="material-icons md-18">visibility_off</i></a>
              </div>
              <div id="confPass" class="input-field ">
                <input id="conf_pass" type="password" class="validate" required="">
                <label for="conf_pass">Confirm Password</label>
              </div>
              <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="term" name="example1" required="">
                <label class="custom-control-label t_and_c" for="term">By creating an account, you agree to our <a href="#" data-toggle="modal" data-target="#terms_of_service"> Terms of Service </a>and Privacy Policy</label>
              </div>
            </div>
            
            <div class="alert alert-danger hide"></div>
            
            <input type="button" name="next" id="signup_step_one" class="next action-button btn btn-block" value="SIGN UP" />
          </fieldset>
          
          <fieldset id="step_two">
            <div class="input-fields-div autoMargin">
              <h4 class="form-label">WHAT IS YOUR <br><span>NATIVE PLACE?</span></h4>
              <div class="input-field">
                <input type="text" id="native_place" class="autocomplete" required="">
                <label for="autocomplete-input">Native Place</label>
              </div><br>
              <h4 class="form-label">WHAT IS YOUR <br><span>CURRENT CITY?</span></h4>
              
              <div class="input-field">
                <input id="current_loc" type="text" class="validate" required="">
                <label for="current_loc">Current City</label>
              </div><br>
            </div>
            
            <div class="alert alert-danger hide"></div>
            
            <input type="button" name="next" id="signup_step_two" class=" action-button btn btn-block" value="SAVE THE DETAILS" />
          </fieldset>
        </form>
      </div>
    </div>
  </div>
</section><!-- #intro -->

<!-- Modal -->
<div class="modal fade" id="terms_of_service" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg opacity-animate3">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <div class="modal-header modal-header-bg ">
          <h6 class="modal-title text-center">TERMS AND CONDITIONS</h6>
      </div>
      
      <div class="modal-body">
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
            Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat ad minim veniam, quis nostrud exerci tation ullamcorper.
            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum
            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
            Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum </p>
        <div class="custom-control custom-checkbox text-center">
          <input type="checkbox" class="custom-control-input" id="customCheck" name="example1">
          <label class="custom-control-label" for="customCheck">I agree your Terms and Conditions</label>
        </div>
        <div class="custom-control custom-checkbox text-center" style="    margin-top: 15px;">
          <input type="submit" name="next" class="btn" value="I AGREE" />
        </div>
      </div>
    </div>
  </div>
</div>