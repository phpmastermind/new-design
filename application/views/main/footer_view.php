  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <?php
    if( !$is_guest && !$is_app )
    {
      echo $this->load->view("main/footer_logged_view", array(), TRUE);
    }
  ?>

  <script>
      var _base_url = '<?php echo $base_url?>';
      var _is_app_view = '<?php echo $is_app?>';
      var _is_mobile_view = <?php echo $is_mobile ? 1 : 0?>;
      var _controller = '<?php echo $controller?>';
      var _method = '<?php echo $method?>';
  </script>


    <!-- Uncomment below i you want to use a preloader -->
    <!-- <div id="preloader"></div> -->
    <!-- JavaScript Libraries -->
    <script src="<?php echo base_url();?>assets/lib/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/lib/jquery/jquery-migrate.min.js"></script>
    <script src="<?php echo base_url();?>assets/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo base_url();?>assets/lib/easing/easing.min.js"></script>
    <script src="<?php echo base_url();?>assets/lib/mobile-nav/mobile-nav.js"></script>
    <script src="<?php echo base_url();?>assets/lib/wow/wow.min.js"></script>
    <script src="<?php echo base_url();?>assets/lib/waypoints/waypoints.min.js"></script>
    <script src="<?php echo base_url();?>assets/lib/counterup/counterup.min.js"></script>
    <script src="<?php echo base_url();?>assets/lib/owlcarousel/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>assets/lib/isotope/isotope.pkgd.min.js"></script>
    <script src="<?php echo base_url();?>assets/lib/lightbox/js/lightbox.min.js"></script>
    <script src="<?php echo base_url();?>assets/lib/materialize/materialize.min.js"></script>
    <!-- Template Main Javascript File -->
    <script src="<?php echo base_url();?>assets/js/main.js"></script>
    
    <?php if( $is_guest ):?>
    <script src="<?php echo base_url();?>assets/js/guest.js"></script>
    <?php endif; ?>
</body>

</html>