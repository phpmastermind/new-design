<div class="invitie-family-member" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel">Add family member</h5>
    </div>
    <div class="modal-body">
      <div class="family-member-body">
        <form name="userRequest" id="userRequest">
          <div class="form-group inline-fields clearfix">
            <label class="">First name</label>
            <input type="text" placeholder="First name" id="first_name">
          </div>
          
          <div class="form-group inline-fields clearfix">
            <label class="">Last name</label>
            <input type="text" placeholder="Last name" id="last_name">
          </div>
          
          <div class="form-group inline-fields clearfix username_ctnr">
            <label class="required_field">Username</label>
            <input type="text" placeholder="Username" id="username" maxlength=20>
          </div>
          
          <div class="form-group inline-fields clearfix relations-dropdown">
            <label class="required_field">Relation</label>
            <div class="country-selectgroup">
              <select id="invite_relation">
                <option value="">-- Select Relation --</option>
                <?php
                $relations = array_merge( $this->config->item('relations'), $this->config->item('second_relations') );
                foreach( $relations as $relation => $relation_id ):
                ?>
                <option value="<?php echo $relation_id?>"><?php echo $relation?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          
          <div class="form-group inline-fields clearfix">
            <label class="required_field">Age range</label>
            <div class="country-selectgroup">
              <select id="age_range">
                <?php
                foreach( $this->config->item('age_ranges') as $range_val => $range ):
                ?>
                <option value="<?php echo $range_val?>"><?php echo $range?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          
          <div class="form-group inline-fields clearfix">
            <label>Member alive</label>
            <div class="country-selectgroup">
              <select id="member_alive">
                <option value="1">Yes</option>
                <option value="0">No</option>
              </select>
            </div>
          </div>
          
          <div class="form-group inline-fields clearfix">
            <label>Email</label>
            <input type="text" placeholder="Email Address" id="invite_email">
          </div>
          
          <div class="form-group inline-fields clearfix">
            <label class="">Mobile number</label>
            <input id="phone1" type="tel" max-length="10" placeholder="Only Indian numbers" />
          </div>
          
          <div class="form-group inline-fields clearfix">
            <label class="required_field">Country</label>
            <div class="country-selectgroup">
              <select id="invite_country">
                <option value="IN">India</option>
                <?php
                /*
                $this->load->config('family');
                foreach( $this->config->item('countries') as $c_code => $country ):
                ?>
                <option value="<?php echo $c_code?>"><?php echo $country?></option>
                <?php endforeach;
                */?>
              </select>
            </div>
          </div>
        </form>
          
        <div class="invite_form_errors alert alert-danger hide"></div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="bttn sendrequestbtn">Add to family</button>
    </div>
  </div>
</div>