<?php
switch( $label_nm )
{
  case 'Uncle':
    $whose = "wife";
  break;

  case 'Aunt':
     $whose = "husband";
  break;

  case 'Son':
  case 'Brother':
     $whose = ( in_array($second_relation_id, array(11, 15) ) ) ? "son" : "daughter";
  break;
}
?>
<div class="form-group inline-fields aunt-uncle-dropdown clearfix">
  <label class="required_field">Whose <?php echo $whose ?>?</label>
  <div class="country-selectgroup">
    <select id="associate_aunt_uncle">
      <option value="">-- Select your <?php echo $label_nm ?> --</option>
      <?php
      foreach( $aunt_uncle as $relation ):
      ?>
      <option value="<?php echo $relation->aunt_uncle_id?>"><?php echo $relation->firstName." ".$relation->lastName?></option>
      <?php endforeach; ?>
    </select>
    <input type="hidden" id="associate_aunt_uncle_type" value="<?php echo $label_nm ?>" />
  </div>
</div>
