<?php
if( isset($suggest_relations['relatives']) || isset($suggest_relations['other']) ):
?>
<div class="post-event-list">
  <div class="send-request-body received-request-fullwrap">
    <div class="send-request-title">
        <h1>Recommendation to add</h1>
    </div>
    
    <div class="send-request-body family-member-suggestion">
      <ul>
        <?php
        if( isset($suggest_relations['relatives']) )
        {
          foreach( $suggest_relations['relatives'] as $relative ):
        ?>
        <li id="suggested_<?php echo $relative['id']?>">
          <div class="send-request-list">
            <div class="clearfix">
              <div class="row">
                <div class="col-lg-3">
                  <img src="<?php echo $relative['image']?>" width="45" height="45" />
                </div>
                
                <div class="col-lg-9">
                  <p><?php echo $relative['firstName'] . " " . $relative['lastName'] ?></p>
                  <div class="add-to-family" id="add_to_family_<?php echo $relative['id']?>">
                    <button class="btn btn-primary" data-id="<?php echo $relative['id']?>">Add to family</button>
                    <div class="form-group hide">
                      <select class="form-control" id="relation_id">
                        <option value="">-- Select Relation --</option>
                        <?php
                        foreach( $this->config->item('relations') as $relation => $relation_id ):
                        ?>
                        <option value="<?php echo $relation_id?>"><?php echo $relation?></option>
                        <?php endforeach; ?>
                        <?php
                        foreach( $this->config->item('second_relations') as $relation => $relation_id ):
                        ?>
                        <option value="<?php echo $relation_id?>"><?php echo $relation?></option>
                        <?php endforeach; ?>
                      </select>
                      <br />
                      <button class="btn btn-danger float-right" data-id="<?php echo $relative['id']?>">Cancel</button>
                      <button class="btn btn-success float-right in-family-member" data-id="<?php echo $relative['id']?>">Add</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
        </li>
        <?php
          endforeach;
        }
        ?>
        
        <?php
        if( isset($suggest_relations['other']) ):
          foreach( $suggest_relations['other'] as $other ):
        ?>
        <li>
          <div class="send-request-list">
            <div class="clearfix">
              <div class="row">
                <div class="col-lg-3">
                  <img src="<?php echo $other['image']?>" width="45" height="45" />
                </div>
                
                <div class="col-lg-9">
                  <p><?php echo $other['firstName'] . " " . $other['lastName'] ?></p>
                  <div class="add-to-family" id="add_to_family_<?php echo $other['id']?>">
                    <button class="btn btn-primary" data-id="<?php echo $other['id']?>">Add to family</button>
                    <div class="form-group hide">
                      <select class="form-control" id="relation_id">
                        <option value="">-- Select Relation --</option>
                        <?php
                        foreach( $this->config->item('relations') as $relation => $relation_id ):
                        ?>
                        <option value="<?php echo $relation_id?>"><?php echo $relation?></option>
                        <?php endforeach; ?>
                        <?php
                        foreach( $this->config->item('second_relations') as $relation => $relation_id ):
                        ?>
                        <option value="<?php echo $relation_id?>"><?php echo $relation?></option>
                        <?php endforeach; ?>
                      </select>
                      <br />
                      <button class="btn btn-danger float-right" data-id="<?php echo $other['id']?>">Cancel</button>
                      <button class="btn btn-success float-right in-family-member" data-id="<?php echo $other['id']?>">Add</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
        </li>
        <?php
          endforeach;
        endif;
        ?>
      </ul>
    </div>
  </div>
</div>
<?php endif; ?>

<div class="post-event-list">
  <?php if( isset($top_ten_tags) && $top_ten_tags ): ?>
  <div class="send-request-body received-request-fullwrap">
    <div class="send-request-title">
        <h1>Top Ten Tags</h1>
    </div>
    
    <div class="send-request-body">
      <ul>
        <?php foreach( $top_ten_tags as $tag ): ?>
        <li>
          <div class="send-request-list">
            <div class="sendlist-emailphone">
              <a href="<?php echo base_url() ?>home/?tag=<?php echo $tag->post_tag_id?>"><?php echo $tag->tag?> (<?php echo $tag->total_posts?>)</a>
            </div>
          </div>
        </li>
        <?php endforeach; ?>
      </ul>
    </div>
  </div>
  <?php endif; ?>
  
  <?php if( isset($recent_tags) && $recent_tags ): ?>
  <div class="send-request-body received-request-fullwrap">
    <div class="send-request-title">
        <h1>Recent Tags</h1>
    </div>
    
    <div class="send-request-body">
      <ul>
        <?php foreach( $recent_tags as $tag ): ?>
        <li>
          <div class="send-request-list">
            <div class="sendlist-emailphone">
              <a href="<?php echo base_url() ?>home/?tag=<?php echo $tag->post_tag_id?>"><?php echo $tag->tag;?></a>
            </div>
          </div>
        </li>
        <?php endforeach; ?>
      </ul>
    </div>
  </div>
  <?php endif; ?>
</div>