<div class="bigbanyan-footer-fullwrap">
  <div class="footer-network">
    <div class="container">
      <div class="row">
        <div class="col-lg-5 col-md-5 col-xs-12">
          <div class="copyright">
              <p>&copy; 2019
                  <a href="userdashboard.html">MagicRelations Network</a>. All Rights Reserved
              </p>
          </div>
        </div>
        <div class="col-lg-7 col-md-7 col-xs-12">
          <div class="footer-nav">
            <ul>
              <li>
                <a href="javascript:void(0)" data-toggle="modal" data-target="#privacypolicy" data-backdrop="static" data-keyboard="false">Privacy</a>
              </li>
              <li>
                <a href="#" data-toggle="modal" data-target="#termscondition" data-backdrop="static" data-keyboard="false">Terms and Conditions</a>
              </li>
              <li>
                <a href="javascript:void(0)" data-toggle="modal" data-target="#aboutus" data-backdrop="static" data-keyboard="false">About</a>
              </li>
              <li>
                <a href="javascript:void(0)" data-toggle="modal" data-target="#feedback" data-backdrop="static" data-keyboard="false">Feedback</a>
              </li>
              <li>
                <a href="javascript:void(0);">Contact Us</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Terms and Condition -->
<div class="terms-condition">
  <div class="modal" id="termscondition">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Terms and Conditions</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="terms-content">
            <h4>Lorem ipsum dolor sit amet</h4>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
                massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem
                ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
                massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
            </p>
            <h4>Lorem ipsum dolor sit amet</h4>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
                massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem
                ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
                massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
            </p>
            <h4>Lorem ipsum dolor sit amet</h4>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
                massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem
                ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
                massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
            </p>
            <h4>Lorem ipsum dolor sit amet</h4>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
                massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem
                ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
                massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
            </p>
          </div>
        </div>
        <div class="modal-footer text-center">
            <button type="button" class="btn acceptbttn" data-dismiss="modal">Close</button>
        </div>

      </div>
    </div>
  </div>
</div>

<!-- About Us -->
<div class="terms-condition">
  <div class="modal" id="aboutus">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">About Us</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="terms-content">
            <h4>Lorem ipsum dolor sit amet</h4>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
                massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem
                ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
                massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
            </p>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
                massa.
            </p>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
                massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem
                ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.
            </p>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
                massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem
                ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
                massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
            </p>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
                massa.
            </p>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
                massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem
                ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.
            </p>

          </div>
        </div>
        <div class="modal-footer text-center">
            <button type="button" class="btn acceptbttn" data-dismiss="modal">Close</button>
        </div>

      </div>
    </div>
  </div>
</div>

<!-- Privacy Policy -->
<div class="privacy-policy-wrap">
  <div class="modal" id="privacypolicy">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Privacy & Policy</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="privacy-content">
            <h4>Lorem ipsum dolor sit amet</h4>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
                massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem
                ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
                massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
            </p>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
                massa.
            </p>
            <p>Please send an email to
              <a href="javascript:void(0)">info@bigbanyan.com</a>
            </p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn acceptbttn" data-dismiss="modal">Close</button>
        </div>

      </div>
    </div>
  </div>
</div>

<!-- Feedback -->
<div class="privacy-policy-wrap feedback-container">
  <div class="modal" id="feedback">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Feedback</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <div class="modal-body">
          <div id="feedback_form_container">
            <div class="form-group">
              <label for="email" class="required_field">Subject:</label>
              <input type="text" class="form-control" id="subject" autocomplete="off" />
            </div>
            
            <div class="form-group">
              <label for="password" class="required_field">Feedback:</label>
              <textarea class="form-control" id="feedback_text"></textarea>
            </div>
            
            <div class="form-group text-right">
              <button type="submit" class="btn btn-success" id="submit_feedback_form">Submit</button>
            </div>
          </div>
          
          <div class="alert alert-success hide">Your feedback has been sent successfully.</div>
          <div class="alert alert-danger hide"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Contact Us -->
<div class="privacy-policy-wrap">
  <div class="modal" id="contactus">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Contact Us</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="privacy-content">
            <h4>Lorem ipsum dolor sit amet</h4>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
                massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem
                ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
                massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
            </p>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
                massa.
            </p>
            <p>
                Please send an email to
                <a href="javascript:void(0)">info@bigbanyan.com</a>
            </p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn acceptbttn" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="stop" class="scrollTop">
  <span>
    <a href="javascript:void(0);">
      <i class="fas fa-arrow-up"></i>
    </a>
  </span>
</div>