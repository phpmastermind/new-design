<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Magic Relations</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="MagicRelations, Magic, Relations, magic, relation, family tree, family, tree, relations">
    <meta content="" name="Manage your family tree, family members and more">
    <!-- Favicons -->
    <link href="<?php echo base_url();?>assets/img/favicon.png" rel="icon">
    <link href="<?php echo base_url();?>assets/img/apple-touch-icon.png" rel="apple-touch-icon">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,500,600,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">
    <!-- Bootstrap CSS File -->
    <link href="<?php echo base_url();?>assets/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Libraries CSS Files -->
    <link href="<?php echo base_url();?>assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/lib/animate/animate.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/lib/lightbox/css/lightbox.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/lib/materialize/materialize.min.css">
    
    <!-- Main Stylesheet File -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/magicrelations.css" rel="stylesheet">
    
</head>

<body>

<body class="<?php echo $is_mobile ? 'mobile_view' : '' ?>">
  <?php
  if( !$is_guest && !$is_app )
  {
    echo $this->load->view("main/header_logged_view", array(), TRUE);
  }
  ?>
    