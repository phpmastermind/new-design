<div class="side-bar-image">
  <div class="user-data">
    <div class="user-profile">
        <div class="userbg">
            <div class="user-picture">
                <img src="<?php echo getUserImage($this->session->userdata('userId')) ; ?>" alt="image">
            </div>
        </div>
        <div class="user-specs">
            <h3><?php echo $this->session->userdata('username');?></h3>
            <span>India</span>
        </div>
    </div>
    <?php /* ?>
    <div class="user-fw-select">
        <ul>
            <li>
                <h4>Following</h4>
                <span>34</span>
            </li>
            <li>
                <h4>Followers</h4>
                <span>155</span>
            </li>
        </ul>
    </div>
    <div class="user-viewprofile">
      <ul>
        <li>
          <a href="javascript:void(0)">View Profile</a>
        </li>
      </ul>
    </div>
    <?php */ ?>
  </div>
  <div class="categories-list">
      <div class="category-title">
          <h1>Categories</h1>
      </div>
      <div class="categorybody">
        <ul>
          <li>
              <a class="mypost" href="<?php echo base_url() ?>posts/myposts">My Posts</a>
          </li>
          <li>
              <a class="myfamily" href="<?php echo base_url() ?>posts/relations">My Family</a>
          </li>
          <li>
              <a class="myprofile" href="<?php echo base_url() ?>profile">Profile</a>
          </li>
        </ul>
      </div>
  </div>
  
  <?php /* ?>
  <div class="recent-peoples">
      <div class="recent-people-title">
          <h1>Peoples</h1>
      </div>
      <div class="recent-people-content">
        <ul>
            <li>
                <div class="people-list">
                    <div class="addlist">
                        <figure>
                            <img src="<?php echo base_url()?>assets/tree/images/profile-picture.jpg" alt="image">
                        </figure>
                        <h1>John Doe</h1>
                    </div>
                    <div class="addbttn-group">
                        <button type="button" class="addbtn">
                            <i class="fas fa-user-plus"></i>
                        </button>
                    </div>
                </div>
            </li>
            <li>
                <div class="people-list">
                    <div class="addlist">
                        <figure>
                            <img src="<?php echo base_url()?>assets/tree/images/profile-2.jpg" alt="image">
                        </figure>
                        <h1>Anna Sthesia</h1>
                    </div>
                    <div class="addbttn-group">
                        <button type="button" class="addbtn">
                            <i class="fas fa-user-plus"></i>
                        </button>
                    </div>
                </div>
            </li>
            <li>
                <div class="people-list">
                    <div class="addlist">
                        <figure>
                            <img src="<?php echo base_url()?>assets/tree/images/profile-picture.jpg" alt="image">
                        </figure>
                        <h1>John Doe</h1>
                    </div>
                    <div class="addbttn-group">
                        <button type="button" class="addbtn">
                            <i class="fas fa-user-plus"></i>
                        </button>
                    </div>
                </div>
            </li>
            <li>
              <div class="people-list">
                  <div class="addlist">
                      <figure>
                          <img src="<?php echo base_url()?>assets/tree/images/profile-2.jpg" alt="image">
                      </figure>
                      <h1>John Doe</h1>
                  </div>
                  <div class="addbttn-group">
                      <button type="button" class="addbtn active">
                          <i class="fas fa-user-plus"></i>
                      </button>
                  </div>
              </div>
            </li>
        </ul>
      </div>
  </div>
  <?php */?>
</div>