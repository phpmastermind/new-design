<div class="user-header">
  <div class="container">
    <div class="header-div">
      <div class="user-logo">
        <a href="<?php echo base_url() ?>home">Magic Relations</a>
      </div>
      <nav>
        <ul>
          <li class="<?php echo ($controller == 'home' && $method == '') ? 'active' : '' ?>">
            <a href="<?php echo base_url() ?>home">Home</a>
          </li>
          <li>
            <a href="<?php echo base_url() ?>add_family_member">Add Family Member</a>
          </li>
          <li class="<?php echo ($controller == 'home' && $method == 'tree') ? 'active' : '' ?>">
            <a href="<?php echo base_url() ?>home/tree">Family Tree</a>
          </li>
        </ul>
      </nav>
      <div class="menu-btn">
        <a href="#">
          <i class="fa fa-bars"></i>
        </a>
      </div>
      <div class="notifcationallicon">
        <ul>
          <li>
            <div class="search-box-filter-wrap">
              <form action="<?php echo base_url()?>search" method="get" id="header_search_form">
                <input type="text" placeholder="Search" name="keyword" id="search_keyword" autocomplete="off" />
                <i class="fas fa-search"></i>
              </form>
            </div>
          </li>
          <?php /* ?>
          <li class="dropdown">
            <a class="notificationshow" href="#" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fas fa-bell"></i>
            </a>
            <div class="bignotification-box dropdown-menu" ria-labelledby="dropdownMenuButton">
                <div class="bignotification-title">
                    <h1>Notification</h1>
                    <a href="javascript:void(0);" title="">Clear all</a>
                </div>
                <div class="bignotify-list-box">
                    <div class="bignotify-list-details">
                        <div class="bignotify-user-img">
                            <img src="<?php echo base_url() ?>assets/images/profile-picture.jpg" alt="image">
                        </div>
                        <div class="bignotify-user-content">
                            <h1>
                                <a href="javascript:void(0);">Jessica William</a>
                                Comment on your post
                            </h1>
                            <span>2 min ago</span>
                        </div>
                    </div>
                    <div class="bignotify-list-details">
                        <div class="bignotify-user-img">
                            <img src="<?php echo base_url() ?>assets/images/profile-picture.jpg" alt="image">
                        </div>
                        <div class="bignotify-user-content">
                            <h1>
                                <a href="#">Jessica William</a>
                                Comment on your post
                            </h1>
                            <span>2 min ago</span>
                        </div>
                    </div>
                    <div class="bignotify-list-details">
                        <div class="bignotify-user-img">
                            <img src="<?php echo base_url() ?>assets/images/profile-picture.jpg" alt="image">
                        </div>
                        <div class="bignotify-user-content">
                            <h1>
                                <a href="#">Jessica William</a>
                                Comment on your post
                            </h1>
                            <span>2 min ago</span>
                        </div>
                    </div>
                    <div class="bignotify-list-details">
                        <div class="bignotify-user-img">
                            <img src="<?php echo base_url() ?>assets/images/profile-picture.jpg" alt="image">
                        </div>
                        <div class="bignotify-user-content">
                            <h1>
                                <a href="#">Jessica William</a>
                                Comment on your post
                            </h1>
                            <span>2 min ago</span>
                        </div>
                    </div>
                </div>
                <div class="view-all-notification">
                    <a href="#">View all notification</a>
                </div>
            </div>
          </li>
          <?php */?>
        </ul>
      </div>
      <div class="user-account">
          <div class="user-info" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <img src="<?php echo getUserImage($this->session->userdata('userId')) ; ?>" alt="icon">
              <a href="#" title="<?php echo $this->session->userdata('username');?>"><?php echo $this->session->userdata('username');?> <i class="fas fa-chevron-down"></i></a>
          </div>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="<?php echo base_url()?>profile">My Profile</a>
              <a class="dropdown-item" href="<?php echo base_url()?>profile/change_password">Change Password</a>
              <a class="dropdown-item" href="<?php echo base_url()?>userlogin/logout">Logout</a>
          </div>
      </div>
    </div>
  </div>
</div>