<?php

if(!defined('BASEPATH')) exit('No direct script access allowed');

function send_otp_sms( $otp, $mobile_number )
{
  $mg = "Your OTP to login MagicRelations is {$otp}";
  
  send_sms( $mobile_number, $mg, $otp );
}

function send_forgot_password_sms( $otp, $mobile_number )
{
  $mg = "Your reset password OTP for MagicRelations is {$otp}";
  
  send_sms( $mobile_number, $mg, $otp );
}

function send_sms( $mobile_number, $message, $otp = NULL )
{
  $CI =& get_instance();
  $CI->load->config('sms');
  
  $api        = $CI->config->item('api_path');
  $authkey    = $CI->config->item('auth_key');
  $otpLength  = $CI->config->item('otp_length');
  $sender     = $CI->config->item('sender');
  $receiver   = $mobile_number;
  $smsmessage = urlencode( $message );

  $sms_api  = $api;
  $sms_api .= "?otp_length={$otpLength}";
  $sms_api .= "&authkey={$authkey}";
  $sms_api .= "&message={$smsmessage}";
  $sms_api .= "&sender={$sender}";
  $sms_api .= "&mobile={$receiver}";
  
  if( $otp )
  {
    $sms_api .= "&otp={$otp}";
  }

  $ch = curl_init();
  
  curl_setopt( $ch, CURLOPT_URL, $sms_api );
  curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
  curl_setopt( $ch, CURLOPT_TIMEOUT, 10 );
  
  $output = curl_exec( $ch );
  curl_close( $ch );
 
  return $output;
}

?>