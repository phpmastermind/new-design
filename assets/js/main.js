(function($)
{
  "use strict";

  // Preloader (if the #preloader div exists)
  $(window).on('load', function()
  {
    if ($('#preloader').length) {
      $('#preloader').delay(100).fadeOut('slow', function() {
        $(this).remove();
      });
    }
  });


  // Back to top button
  $(window).scroll(function()
  {
    if ($(this).scrollTop() > 100) {
      $('.back-to-top').fadeIn('slow');
    } else {
      $('.back-to-top').fadeOut('slow');
    }
  });
  $('.back-to-top').click(function() {
    $('html, body').animate({ scrollTop: 0 }, 1500, 'easeInOutExpo');
    return false;
  });

  // Initiate the wowjs animation library
  new WOW().init();

  // Header scroll class
  $(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
      $('#header').addClass('header-scrolled');
    } else {
      $('#header').removeClass('header-scrolled');
    }
  });

  if ($(window).scrollTop() > 100)
  {
    $('#header').addClass('header-scrolled');
  }

  // Smooth scroll for the navigation and links with .scrollto classes
  $('.main-nav a, .mobile-nav a, .scrollto').on('click', function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      if (target.length)
      {
        var top_space = 0;

        if ($('#header').length) {
          top_space = $('#header').outerHeight();

          if (!$('#header').hasClass('header-scrolled')) {
            top_space = top_space - 40;
          }
        }

        $('html, body').animate({
          scrollTop: target.offset().top - top_space
        }, 1500, 'easeInOutExpo');

        if ($(this).parents('.main-nav, .mobile-nav').length) {
          $('.main-nav .active, .mobile-nav .active').removeClass('active');
          $(this).closest('li').addClass('active');
        }

        if ($('body').hasClass('mobile-nav-active')) {
          $('body').removeClass('mobile-nav-active');
          $('.mobile-nav-toggle i').toggleClass('fa-times fa-bars');
          $('.mobile-nav-overly').fadeOut();
        }
        return false;
      }
    }
  });

  // Navigation active state on scroll
  var nav_sections = $('section');
  var main_nav = $('.main-nav, .mobile-nav');
  var main_nav_height = $('#header').outerHeight();

  $(window).on('scroll', function() {
    var cur_pos = $(this).scrollTop();

    nav_sections.each(function() {
      var top = $(this).offset().top - main_nav_height,
      bottom = top + $(this).outerHeight();

      if (cur_pos >= top && cur_pos <= bottom) {
        main_nav.find('li').removeClass('active');
        main_nav.find('a[href="#' + $(this).attr('id') + '"]').parent('li').addClass('active');
      }
    });
  });

  // jQuery counterUp (used in Whu Us section)
  $('[data-toggle="counter-up"]').counterUp({
    delay: 10,
    time: 1000
  });

  // Porfolio isotope and filter
  $(window).on('load', function() {
    var portfolioIsotope = $('.portfolio-container').isotope({
      itemSelector: '.portfolio-item'
    });
    $('#portfolio-flters li').on('click', function() {
      $("#portfolio-flters li").removeClass('filter-active');
      $(this).addClass('filter-active');

      portfolioIsotope.isotope({ filter: $(this).data('filter') });
    });
  });

  // Testimonials carousel (uses the Owl Carousel library)
  $(".testimonials-carousel").owlCarousel({
    autoplay: true,
    dots: true,
    loop: true,
    items: 1
  });

  // Clients carousel (uses the Owl Carousel library)
  $(".clients-carousel").owlCarousel({
    autoplay: true,
    dots: true,
    loop: true,
    responsive: {
      0: { items: 2 },
      768: { items: 4 },
      900: { items: 6 }
    }
  });

})(jQuery);



$('.menu').click (function(){
  $(this).toggleClass('open');
});

$(document).click(function(event){
  if (event.target.class === 'menu') {
  } else {
      $(".menu").removeClass("open"); 
  }
});


function show1() {
    document.getElementById('div1').style.display = 'block';
    document.getElementById('div4').style.display = 'block';
    document.getElementById('div2').style.display = 'none';
    document.getElementById('div3').style.display = 'none';
    document.getElementById('div5').style.display = 'none';
}

function show2() {
    document.getElementById('div2').style.display = 'block';
    document.getElementById('div6').style.display = 'block';
    document.getElementById('div1').style.display = 'none';
    document.getElementById('div3').style.display = 'none';
    document.getElementById('div7').style.display = 'none';
}

function show3() {
    document.getElementById('div2').style.display = 'none';
    document.getElementById('div1').style.display = 'none';
    document.getElementById('div3').style.display = 'block';
    document.getElementById('div4').style.display = 'none';
}

var allPasswordInp = [];

$("input[type=password]").each(function(idx, ele) {
    allPasswordInp.push(ele)
})

function showPassword()
{
  var iconText = $(".showPassword:eq(0) i").text();
  var input_type = (iconText == "visibility_off") ? "text" : "password";
  
  if (input_type == "text") {
      $(".showPassword i").text("visibility");
  } else {
      $(".showPassword i").text("visibility_off");
  }

  $.each(allPasswordInp, function(idx, ele)
  {
    $(ele).attr("type", input_type);
  });
}


var navListItems = $('div.setup-panel div a'),
    allWells = $('.setup-content'),
    allNextBtn = $('.nextBtn');

allWells.hide();

navListItems.click(function(e) {
    e.preventDefault();
    var $target = $($(this).attr('href')),
        $item = $(this);

    if (!$item.hasClass('disabled')) {
        navListItems.removeClass('btn-primary').addClass('btn-default');
        $item.addClass('btn-primary');
        allWells.hide();
        $target.show();
        $target.find('input:eq(0)').focus();
    }
});

allNextBtn.click(function() {
    var curStep = $(this).closest(".setup-content"),
        curStepBtn = curStep.attr("id"),
        nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
        curInputs = curStep.find("input[type='text'],input[type='url']"),
        isValid = true;

    $(".form-group").removeClass("has-error");
    for (var i = 0; i < curInputs.length; i++) {
        if (!curInputs[i].validity.valid) {
            isValid = false;
            $(curInputs[i]).closest(".form-group").addClass("has-error");
        }
    }

    if (isValid)
        nextStepWizard.removeAttr('disabled').trigger('click');
});

$('div.setup-panel div a.btn-primary').trigger('click');



//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches

$(".previous").click(function() {
    if (animating) return false;
    animating = true;

    current_fs = $(this).parent();
    previous_fs = $(this).parent().prev();

    //de-activate current step on progressbar
    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

    //show the previous fieldset
    previous_fs.show();
    //hide the current fieldset with style
    current_fs.animate({ opacity: 0 }, {
        step: function(now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale previous_fs from 80% to 100%
            scale = 0.8 + (1 - now) * 0.2;
            //2. take current_fs to the right(50%) - from 0%
            left = ((1 - now) * 50) + "%";
            //3. increase opacity of previous_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({ 'left': left });
            previous_fs.css({ 'transform': 'scale(' + scale + ')', 'opacity': opacity });
        },
        duration: 800,
        complete: function() {
            current_fs.hide();
            animating = false;
        },
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
    });
});

$(".submit").click(function() {
    return false;
})


///////////////**************** button effect**************//////////////

const ButtonHover = {
  button: document.querySelector('.button'),
  elWidth: 0,
  elHeight: 0,
  cursorX: 0,
  cursorY: 0,
  elCenterX: 0,
  elCenterY: 0,

  init() {
    this.elWidth = this.button.offsetWidth;
    this.elHeight = this.button.offsetHeight;
    this.button.addEventListener('mousemove', e => this.animate(e));
  },

  animate(e) {
    let cord = e.target.getBoundingClientRect();
    this.cursorX = e.x;
    this.cursorY = e.y;
    this.elCenterX = cord.left + cord.width / 2;
    this.elCenterY = cord.top + cord.height / 2;
    let y = this.elCenterY - this.cursorY;
    let x = this.elCenterX - this.cursorX;

    let theta = Math.atan2(y, x);
    let angle = theta * 180 / Math.PI - 90;
    if (angle < 0) {
      angle = angle + 360;
    }

    this.button.style.transform = 'translateX(' + -x * 0.05 + 'px) rotateX(' + -y * 0.1 + 'deg) rotateY(' + x * 0.1 + 'deg)';
    this.button.style.boxShadow = x * 0.2 + "px " + y * 0.3 + "px 28px rgba(0,0,0,0.25)";

  } };


ButtonHover.init();

/******* City Dropdown *************/

// Or with jQuery

$(document).ready(function() {
    $('input.autocomplete').autocomplete({
        data: {
            "Mumbai": null,
            "Nashik": null,
            "Pune": null
        },
    });
});

/******************** Modal Effets ************/