
// Signup form first step vars
var username      = "";
var user_email    = "";
var password      = "";
var conf_password = "";
var native_place  = "";
var current_city  = "";
  
$(document).ready(function ()
{
  $(document).on('click touch', '.loginForm #login_user', function()
  {
    __user_login();
  });
  
  $(document).on('click touch', '.login-send-otp #send_otp', function()
  {
    __login_send_otp();
  });
  
  $(document).on('click touch', '#login_with_otp #login_by_otp', function()
  {
    __login_by_otp();
  });
  
  $(document).on('click touch', '#login_with_otp #resend_login_otp', function()
  {
    $("#login_with_otp #resend_login_otp").text('Resending...');
    
    __login_send_otp();
    
    setTimeout(function(){ $("#login_with_otp #resend_login_otp").text('Resend OTP'); }, 2000);
  });
  
  $(document).on('click touch', '#forgot_password #submit_forgot_password', function()
  {
    __forgot_password_otp();
  });
  
  $(document).on('click touch', '#reset_password #reset_password_btn', function()
  {
    __reset_password();
  });
  
  //jQuery time
  var current_fs, next_fs, previous_fs; //fieldsets
  var left, opacity, scale; //fieldset properties which we will animate
  var animating; //flag to prevent quick multi-click glitches

  $(document).on('click touch', '.user_signup_form #signup_step_one', function()
  {
    $(".user_signup_form #step_one .alert").html("").addClass('hide');
    
    username      = $(".user_signup_form #user_name").val();
    user_email    = $(".user_signup_form #user_email").val();
    password      = $(".user_signup_form #pass_word").val();
    conf_password = $(".user_signup_form #conf_pass").val();
    
    var error_text  = "";
    var regex       = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    
    if( $.trim(username) == "" )
    {
      error_text = error_text + "Please provide Username <br/>";
    }
    
    if( $.trim(user_email) == "" )
    {
      error_text = error_text + "Please provide Email <br/>";
    }
    
    if( $.trim(password) == "" )
    {
      error_text = error_text + "Please provide Password <br/>";
    }
    
    if( $.trim(conf_password) == "" )
    {
      error_text = error_text + "Please provide Confirm Password <br/>";
    }
    
    if( $(".user_signup_form #term").prop("checked") == false  )
    {
      error_text = error_text + "Please accept Terms of Service <br/>";
    }
    
    if( $.trim(user_email) && !regex.test(user_email) )
    {
      error_text = error_text + "Email address provided is not a valid <br />";
    }
    
    if( password != conf_password )
    {
      error_text = error_text + "Password and Confirm Password are not same <br />";
    }
    
    if( error_text != "" )
    {
      $(".user_signup_form #step_one .alert").html( error_text ).removeClass('hide');
      return;
    }
    
    if (animating) return false;
    animating = true;

    current_fs = $(this).parent();
    next_fs = $(this).parent().next();

    //activate next step on progressbar using the index of next_fs
    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

    //show the next fieldset
    next_fs.show();
    
    //hide the current fieldset with style
    current_fs.animate({ opacity: 0 },
    {
      step: function(now, mx)
      {
          //as the opacity of current_fs reduces to 0 - stored in "now"
          //1. scale current_fs down to 80%
          scale = 1 - (1 - now) * 0.2;
          //2. bring next_fs from the right(50%)
          left = (now * 50) + "%";
          //3. increase opacity of next_fs to 1 as it moves in
          opacity = 1 - now;
          current_fs.css({
              'transform': 'scale(' + scale + ')',
              'position': 'absolute'
          });
          next_fs.css({ 'left': left, 'opacity': opacity });
      },
      duration: 800,
      complete: function() {
        current_fs.hide();
        animating = false;
      },
      
      //this comes from the custom easing plugin
      easing: 'easeInOutBack'
    });
  });
  
  $(document).on('click touch', '.user_signup_form #signup_step_two', function()
  {
    $(".user_signup_form #step_two .alert").html("").addClass('hide');
    $(".user_signup_form #step_two #signup_step_two").attr('disabled', true).text('Saving...');
    
    native_place = $(".user_signup_form #native_place").val();
    current_city = $(".user_signup_form #current_loc").val();
    
    var _error_text  = "";
    
    if( $.trim(native_place) == "" )
    {
      _error_text = _error_text + "Please provide Native Place<br/>";
    }
    
    if( $.trim(current_city) == "" )
    {
      _error_text = _error_text + "Please provide Current City <br/>";
    }
    
    if( _error_text != "" )
    {
      $(".user_signup_form #step_two .alert").html( error_text ).removeClass('hide');
      return;
    }
    
    __register_user();
  });
  
});

var __user_login = function()
{
  $(".loginForm .alert").html("").addClass("hide");
  
  var username = $(".loginForm #username").val();
  var password = $(".loginForm #password").val();
  
  if( !$.trim(username) || !$.trim(password) )
  {
    $(".loginForm .alert.hide").html("Please complete all fields").removeClass("hide");
    return;
  }
  
  $.ajax({
    url: _base_url + "account/user_login",
    type: 'POST',
    dataType: 'json',
    data: { 'username' : username, 'password' : password },
    error: function( jqXHR, textStatus, errorThrown )
    {
      $(".loginForm .alert.hide").html( "Something went wrong. Please try again later." ).removeClass("hide");
    },
    success: function( data ) 
    {
      if( typeof data.error != "undefined" && data.error != "" )
      {
        $(".loginForm .alert.hide").html( data.error ).removeClass("hide");
      }
      else
      {
        window.location =_base_url + data.path;
      }
    }
  });
};

var __login_send_otp = function()
{
  $(".login-send-otp .alert").html("").addClass("hide");
  $(".login-send-otp #send_otp").attr('disabled', true).text('Sending...');
  
  var mobile = $(".login-send-otp #user_phone").val();
  
  if( !$.trim(mobile) )
  {
    $(".login-send-otp .alert.hide").html("Please provide Mobile No.").removeClass("hide");
    $(".login-send-otp #send_otp").attr('disabled', false).text('Send OTP');
    return;
  }
  
  $.ajax({
    url: _base_url + "account/login_send_otp",
    type: 'POST',
    dataType: 'json',
    data: { 'mobile' : mobile },
    error: function()
    {
      $(".login-send-otp .alert.hide").html( "Something went wrong. Please try again later." ).removeClass("hide");
      $(".login-send-otp #send_otp").attr('disabled', false).text('Send OTP');
    },
    success: function( data ) 
    {
      if( typeof data.error != "undefined" && data.error != "" )
      {
        $(".login-send-otp .alert.hide").html( data.error ).removeClass("hide");
        $(".login-send-otp #send_otp").attr('disabled', false).text('Send OTP');
      }
      else
      {
        $(".login-send-otp #send_otp").attr('disabled', false).text('Send OTP');
        
        $("#login_with_otp #login_otp").val( data.otp );
        $("#login_with_otp #login_otp_user").val( data.user_id );
        
        $("#div2 #div6").hide();
        $("#div2 #div7").show();
      }
    }
  });
};

var __login_by_otp = function()
{
  $("#login_with_otp .alert").html("").addClass("hide");
  $("#login_with_otp #login_by_otp").attr('disabled', true).text('Submitting...');
  
  var user_otp    = $("#login_with_otp #user_otp_fg").val();
  var system_otp  = $("#login_with_otp #login_otp").val();
  var system_user = $("#login_with_otp #login_otp_user").val();
  
  var error_txt = "";
  if( !$.trim(user_otp) )
  {
    error_txt = "Please provide OTP you received";
  }
  else if( user_otp != system_otp )
  {
    error_txt = "OTP you provided is not valid";
  }
  
  if( error_txt != "" )
  {
    $("#login_with_otp .alert").html( error_txt ).removeClass("hide");
    $("#login_with_otp #login_by_otp").attr('disabled', false).text('Submit OTP');
    return;
  }
  
  $.ajax({
    url: _base_url + "account/otp_login",
    type: 'POST',
    dataType: 'json',
    data: { 'user_otp' : user_otp, 'system_otp': system_otp, 'system_user': system_user },
    error: function()
    {
      $("#login_with_otp .alert").html( "Something went wrong. Please try again later." ).removeClass("hide");
      $("#login_with_otp #login_by_otp").attr('disabled', false).text('Submit OTP');
    },
    success: function( data ) 
    {
      if( typeof data.error != "undefined" && data.error != "" )
      {
        $("#login_with_otp .alert").html( data.error ).removeClass("hide");
        $("#login_with_otp #login_by_otp").attr('disabled', false).text('Submit OTP');
      }
      else
      {
        window.location =_base_url + data.path;
      }
    }
  });
  
};

var __forgot_password_otp = function()
{
  $("#forgot_password .alert").html("").addClass("hide");
  $("#forgot_password #submit_forgot_password").attr('disabled', true).text('Submitting...');
  
  var username = $("#forgot_password #user_name_fg").val();
  
  if( !$.trim(username) )
  {
    $("#forgot_password .alert").html("Please provide Username").removeClass("hide");
    $("#forgot_password #submit_forgot_password").attr('disabled', false).text('Submit');
    return;
  }
  
  $.ajax({
    url: _base_url + "account/forgot_password",
    type: 'POST',
    dataType: 'json',
    data: { 'username' : username },
    error: function()
    {
      $("#forgot_password .alert.hide").html( "Something went wrong. Please try again later." ).removeClass("hide");
      $("#forgot_password #submit_forgot_password").attr('disabled', false).text('Submit');
    },
    success: function( data ) 
    {
      if( typeof data.error != "undefined" && data.error != "" )
      {
        $("#forgot_password .alert.hide").html( data.error ).removeClass("hide");
        $("#forgot_password #submit_forgot_password").attr('disabled', false).text('Submit');
      }
      else
      {
        $('#div2').hide();
        $('#div1').hide();
        $('#div3').hide();
        $('#div4').hide();
        $('#div5').show();
        
        $("#reset_password #user_id").val( data.user_id );
      }
    }
  });
};

var __reset_password = function()
{
  $("#reset_password .alert").html("").addClass("hide");
  $("#reset_password #reset_password_btn").attr('disabled', true).text('Resetting...');
  
  var otp           = $("#reset_password #user_otp").val();
  var user_id       = $("#reset_password #user_id").val();
  var user_pass     = $("#reset_password #user_pass").val();
  var user_pass_con = $("#reset_password #user_pass_con").val();
  var error_txt     = "";
  
  if( !$.trim(otp) || !$.trim(user_pass) || !$.trim(user_pass_con) )
  {
    error_txt = "Please complete fields";
  }
  else if( user_pass != user_pass_con )
  {
    error_txt = "Password and Confirm Password are not same";
  }
  
  if( error_txt != "" )
  {
    $("#reset_password .alert").html( error_txt ).removeClass("hide");
    $("#reset_password #reset_password_btn").attr('disabled', false).text('Reset');
    return;
  }
  
  $.ajax({
    url: _base_url + "account/reset_password",
    type: 'POST',
    dataType: 'json',
    data: { 'otp' : otp, 'user_id': user_id, 'user_pass': user_pass, 'user_pass_con': user_pass_con },
    error: function()
    {
      $("#reset_password .alert.hide").html( "Something went wrong. Please try again later." ).removeClass("hide");
      $("#reset_password #reset_password_btn").attr('disabled', false).text('Reset');
    },
    success: function( data ) 
    {
      if( typeof data.error != "undefined" && data.error != "" )
      {
        $("#reset_password .alert.hide").html( data.error ).removeClass("hide");
        $("#reset_password #reset_password_btn").attr('disabled', false).text('Reset');
      }
      else
      {
        $("#reset_password .alert").html( "Your password reset successfully" ).removeClass("hide alert-danger").addClass("alert-success");
        $("#reset_password #reset_password_btn").attr('disabled', false).text('Reset');
      }
    }
  });
};

var __register_user = function()
{
  $.ajax({
    url: _base_url + "account/user_signup",
    type: 'POST',
    dataType: 'json',
    data: {
      'username'      : username,
      'user_email'    : user_email,
      'password'      : password,
      'conf_password' : conf_password,
      'native_place'  : native_place,
      'current_city'  : current_city
    },
    error: function()
    {
      $(".user_signup_form #step_two .alert").html( "Something went wrong. Please try again later." ).removeClass('hide');
      $(".user_signup_form #step_two #signup_step_two").attr('disabled', false).text('Save the details');
    },
    success: function( data ) 
    {
      if( typeof data.error != "undefined" && data.error != "" )
      {
        $(".user_signup_form #step_two .alert").html( data.error ).removeClass('hide');
        $(".user_signup_form #step_two #signup_step_two").attr('disabled', false).text('Save the details');
      }
      else
      {
        window.location =_base_url + data.path;
      }
    }
  });
};